<?php

class Course_model extends CI_Model
{

    public function getCourse($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_course')->result_array();
        } else {
            return $this->db->get_where('sut_course', ['slang' => $id])->result_array();
        }
    }

    public function getContent($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_course_content')->result_array();
        } else {
            return $this->db->get_where('sut_course_content', ['courseID' => $id])->result_array();
        }
    }
    
    public function getContract($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_course_contract')->result_array();
        } else {
            return $this->db->get_where('sut_course_contract', ['courseID' => $id])->result_array();
        }
    }

    public function getBranch($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_course_branch')->result_array();
        } else {
            return $this->db->get_where('sut_course_branch', ['slang' => $id])->result_array();
        }
    }

    public function getStydy($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_course_content_study')->result_array();
        } else {
            return $this->db->get_where('sut_course_content_study', ['courseID' => $id])->result_array();
        }
    }

    public function getMisstion($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_course_mission')->result_array();
        } else {
            return $this->db->get_where('sut_course_mission', ['courseID' => $id])->result_array();
        }
    }

    public function getPay($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_course_content_pay')->result_array();
        } else {
            return $this->db->get_where('sut_course_content_pay', ['courseID' => $id])->result_array();
        }
    }

    public function getQuestionType($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_course_conent_question_type')->result_array();
        } else {
            return $this->db->get_where('sut_course_conent_question_type', ['courseID' => $id])->result_array();
        }
    }

    public function getQuestion($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_course_conent_question')->result_array();
        } else {
            return $this->db->get_where('sut_course_conent_question', ['questionTypeID' => $id])->result_array();
        }
    }

    public function getCrepes($id = null)
    {
        return $this->db->get('sut_course_conent_prepes')->result_array();
    }

    public function getBranchIcon($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_course_branch_icon')->result_array();
        } else {
            return $this->db->get_where('sut_course_branch_icon', ['courseID' => $id])->result_array();
        }
    }

    public function getBranchContractFooter($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_course_footer_contract')->result_array();
        } else {
            return $this->db->get_where('sut_course_footer_contract', ['courseID' => $id])->result_array();
        }
    }

    public function getShouldContact($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_course_content_should_contact')->result_array();
        } else {
            return $this->db->get_where('sut_course_content_should_contact', ['courseID' => $id])->result_array();
        }
    }
    
    
}