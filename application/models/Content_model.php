<?php

class Content_model extends CI_Model
{

    public function getHome()
    {
        return $this->db->get_where('sut_content', ['pageID' => 1])->result_array();
    }
    
    public function getCourse()
    {
        return $this->db->get_where('sut_content', ['pageID' => 2])->result_array();
    }

    public function getEnrollment()
    {
        return $this->db->get_where('sut_content', ['pageID' => 3])->result_array();
    }

    public function getRegister()
    {
        return $this->db->get_where('sut_register')->result_array();
    }

    public function getLeaveConditions()
    {
        return $this->db->get_where('sut_content', ['pageID' => 4])->result_array();
    }

    public function getCooperative()
    {
        return $this->db->get_where('sut_content', ['pageID' => 5])->result_array();
    }

    public function getTableGrade()
    {
        return $this->db->get('sut_table_grade')->result_array();
    }

    public function getTableGPA($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_table_gpa')->result_array();
        } else {
            return $this->db->get_where('sut_table_gpa', ['tableGroup' => $id])->result_array();
        }
    }

    public function getTableRetry()
    {
        return $this->db->get('sut_table_rentry')->result_array();
    }
}