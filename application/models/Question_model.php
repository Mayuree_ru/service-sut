<?php

class Question_model extends CI_Model
{
    public function getQuestionType($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_question_type')->result_array();
        } else {
            return $this->db->get_where('sut_question_type', ['id' => $id])->result_array();
        }
    }

    public function getQuestion($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_question')->result_array();
        } else {
            return $this->db->get_where('sut_question', ['typeId' => $id])->result_array();
        }
    }

    public function getAnswer($id = null)
    {
        if ($id === null) {
            return $this->db->get('sut_question_answer')->result_array();
        } else {
            return $this->db->get_where('sut_question_answer', ['questionId' => $id])->result_array();
        }
    }
}