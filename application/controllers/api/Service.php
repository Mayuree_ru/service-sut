<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Service extends REST_Controller {

    function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: *");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
        parent::__construct();
        
        $this->load->model('Service_model', 'service');
    
        $this->methods['index_get']['limit'] = 100; // 500 requests per hour per user/key
    }
    
    public function index_get()
    {
        $content = $this->service->getService();
        if ($content) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $content
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
}