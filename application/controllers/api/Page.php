<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Page extends REST_Controller {

    function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: *");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
        parent::__construct();
        
        $this->load->model('Content_model', 'content');
        $this->load->model('Course_model', 'course');
        $this->load->model('Question_model', 'question');
    
        $this->methods['home_get']['limit'] = 100; // 500 requests per hour per user/key
        $this->methods['course_get']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['enrollment_get']['limit'] = 100; // 50 requests per hour per user/key
        $this->methods['leave_conditions_get']['limit'] = 100; // 50 requests per hour per user/key
        $this->methods['cooperative_get']['limit'] = 100; // 50 requests per hour per user/key
        $this->methods['question_get']['limit'] = 100; // 50 requests per hour per user/key
    }
    
    public function pagenotfound()
    {
        $this->response(NULL, REST_Controller::HTTP_NOT_FOUND);
    }
    
    public function home_get()
    {
        $content = $this->content->getHome();
        $data["page"] = "หน้าแรก";
        $i = 0;
        
        if ($content) {
            foreach ($content as $content) {
                $data["section"][$i] = $content;
                $i++;
            }
        }else {
            $data["section"] = [];
        }

        if ($data) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function course_get()
    {
        $content = $this->content->getCourse();
        $course = $this->course->getCourse();
        $data["page"] = "แนะนำหลักสูตร";
        $i = 0;
        
        if ($content) {
            foreach ($content as $content) {
                $data["section"][$i] = $content;
                $i++;
            }
        }else {
            $data["section"] = [];
        }

        if ($course) {
            foreach ($course as $course) {
                $data["course"][$i] = $course;
                $i++;
            }
        }else {
            $data["course"] = [];
        }

        if ($data) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function enrollment_get()
    {
        $content = $this->content->getEnrollment();
        $data["page"] = "การลงทะเบียนเรียน";
        $i = 0;
        
        if ($content) {
            foreach ($content as $content) {
                $data["section"][$i] = $content;
                $j = 0 ;
                if ($content["content"] != null) {
                    if ($content["id"] == 7){
                        $register = $this->content->getRegister();
                        foreach ($register as $register) {
                            $data["section"][$i]["menu_data"][$j]["name"] = $register["title"];
                            $data["section"][$i]["menu_data"][$j]["slung"] = $register["slung"];
                            $j++;
                        }
                    }else {
                        $subcontent =  explode(",",$content["content"]);
                        foreach ($subcontent as $subcontent) {
                            $data["section"][$i]["conent_".$j] = $subcontent;
                            $j++;   
                        }
                   }   
                    // }
                //    $subcontent =  explode(",",$content["content"]);
                //    foreach ($subcontent as $subcontent) {
                //     $data["section"][$i]["conent_".$j] = $subcontent;
                //     $j++;
                //    }           
                }
                $i++;
            }
        }else {
            $data["section"] = [];
        }

        if ($data) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function leave_conditions_get()
    {
        $content = $this->content->getLeaveConditions();
        $data["page"] = "เงื่อนไขการลา";
        $i = 0;
        
        if ($content) {
            foreach ($content as $content) {
                $data["section"][$i] = $content;
                $j = 0 ;
                if ($content["content"] != null) {
                   $subcontent =  explode(",",$content["content"]);
                   foreach ($subcontent as $subcontent) {
                    $data["section"][$i]["conent_".$j] = $subcontent;
                    $j++;
                   }           
                }
                $i++;
            }
        }else {
            $data["section"] = [];
        }

        if ($data) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function cooperative_get()
    {
        $content = $this->content->getCooperative();
        $data["page"] = "การปฏิบัติงานสหกิจศึกษา";
        $i = 0;
        
        if ($content) {
            foreach ($content as $content) {
                $data["section"][$i] = $content;
                $j = 0 ;
                if ($content["content"] != null) {
                   $subcontent =  explode(",",$content["content"]);
                   foreach ($subcontent as $subcontent) {
                    $data["section"][$i]["conent_".$j] = $subcontent;
                    $j++;
                   }           
                }
                $i++;
            }
        }else {
            $data["section"] = [];
        }

        if ($data) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function question_get()
    {
        $memu = array ('การลาระหว่างเรียน /สอบ และพักการศึกษา','การเพิ่ม / ลด / ถอนรายวิชา /เปลี่ยนกลุ่มรายวิชา/ ระยะเวลาที่ลดโดยไม่เสียเงินเต็มจํานวน','การเงิน','การแจ้งจบ การขอ Transcript การขอใบรับรอง');
        $content = $this->question->getQuestionType();
        $data["page"] = "คำถามที่พบบ่อย";
        $data['sub_title'] = $memu;
        $i = 0;
        
        if ($content) {
            foreach ($content as $content) {
                $data["section"][$i] = $content;
                $j = 0;
                $question = $this->question->getQuestion($content['id']);
                foreach ($question as $question) {
                    $data["section"][$i]["question"][$j] = $question;
                    $k = 0 ;
                if ($question["answer"] != null) {
                   $subcontent =  explode(",",$question["answer"]);
                   foreach ($subcontent as $subcontent) {
                    $data["section"][$i]["question"][$j]["answer_".$k] = $subcontent;
                    $k++;
                   }           
                }
                    $j++;
                }
                $i++;
            }
        }else {
            $data["section"] = [];
        }

        if ($data) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function how_to_register_get()
    {
        $content = $this->content->getRegister();
        $data["page_th"] = "ขั้นตอนต่างๆ";
        $data["page_en"] = "how to Registration";
        $i = 0;
        
        if ($content) {
            foreach ($content as $content) {
                $data["section"][$i] = $content;
                $j = 0 ;
                if ($content["content"] != null) {
                   $subcontent =  explode("|",$content["content"]);
                   foreach ($subcontent as $subcontent) {
                    $data["section"][$i]["conent_".$j] = $subcontent;
                    $j++;
                   }           
                }
                $i++;
            }
        }else {
            $data["section"] = [];
        }

        if ($data) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function table_grade_get()
    {
        $data = $this->content->getTableGrade();
        if ($data) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function table_gpa_get()
    {
        $group = $this->get('group');
        if ($group) {
            $data = $this->content->getTableGPA($group);
        }else {
            $data = $this->content->getTableGPA();
        }
        if ($data) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function table_retry_get()
    {
        $table = $this->content->getTableRetry();
        //$data[] = $table;
        $data[0]['titel'] = 'ตัวอย่างที่ 1';
            $data[1]['titel'] = 'ตัวอย่างที่ 2';
        foreach ($table as $table) {
        //     $i = 0;
            if ($table['id']<=3) {
                $data[0]['detail'][] = $table;
            }else {
                $data[1]['detail'][] = $table;
            }
        }
        if ($data) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    
    
}