<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Institute extends REST_Controller {
    function __construct()
    {
        // header('Access-Control-Allow-Origin: *');
        // header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        // header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: *");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
        parent::__construct();
    
        $this->load->model('Course_model', 'course');
    
        $this->methods['index_get']['limit'] = 100; // 500 requests per hour per user/key
        $this->methods['branch_get']['limit'] = 100; // 500 requests per hour per user/key
    }

    public function index_get()
    {
        $course = $this->get('course');
            $department = $this->course->getCourse($course);
            
        if($department){
            foreach ($department as $department) {
                
                $content = $this->course->getContent($department['id']);
                $contract = $this->course->getContract($department['id']);
                $study = $this->course->getStydy($department['id']);
                $misstion = $this->course->getMisstion($department['id']);
                $pay = $this->course->getPay($department['id']);
                $data = $department;
               
                foreach ($content as $content) {
                    //$data[$content["slang"]] = $content;
                    $data[$content["slang"]]['title'] = $content["title"];
                    //$data[$content["slang"]]['text'] = $content["content"];
                    if ($content["content"] != null){
                        $text =  explode("|",$content["content"]);
                        $i=0;
                        foreach ($text as $text) {
                            $data[$content["slang"]]['text'][0]["text_".$i] = $text;
                            $i++;
                        }  
                    }
                    if ($content["image"] != null) {
                        $j = 0 ;
                        $subcontent =  explode(",",$content["image"]);
                        foreach ($subcontent as $subcontent) {
                            $data[$content["slang"]]['image'][0]["image_".$j] = $subcontent;
                            $j++;
                        }           
                    }
                    
                    
                    
                }
                if($study != NULL){
                    $head = [
                        '', 
                        'เรียนอย่างไรให้จบตามเวลา ?',
                        '',
                        'เรียนอย่างไรให้จบตามเวลา ?',
                        'เรียนอย่างไรให้จบตามเวลา ?',
                        'เรียนอย่างไรให้จบตามเวลา ?',
                        'เรียนอย่างไรให้จบตามเวลา ?',
                        '',
                        'เรียนอย่างไรให้จบตามเวลา ?'
                    ];
                    $data['study']["title"] = $head[$department['id']];
                    if($department['id'] == 6) {
                        $data['study']["sub_title"] = "เราเรียนระบบ 3 เทอม แต่ละเทอมจะมีเวลาเพียง 12 สัปดาห์ สิ่งที่ต้องทำให้ได้คือการแบ่งเวลา เวลาเรียน เวลาเล่น เวลาทำกิจกรรมทำทุกอย่างให้สมดุล";
                    }
                    $data['study']['data'] = $study;
                    // $i=0;
                    // foreach ($study as $study){
                    //     $data['study']['data'][$i]['id'] = $study['id'];
                    //     $data['study']['data'][$i]['image'] = $study['image'];
                    //     if ($study['detail'] != NULL){
                    //         $text =  explode("|",$misstion['detail']);
                    //         foreach ($text as $text) {
                    //             $data['study']['data'][$i]['detail_'.$i] = $text;
                    //         }
                    //     }
                    //     $data['study']['data'][$i]['courseID'] = $study['courseID'];
                    //     $i++;
                    // }
                }
                if ($misstion != NULL) {
                    $data['misstion']['title'] ="วิสัยทัศน์/พันธกิจ/ค่านิยม";
                    //$data['misstion']['data'] = $misstion;
                    $k=0;
                    foreach ($misstion as $misstion) {
                        $data['misstion']['data'][$k]['title'] = $misstion['title'];
                        $data['misstion']['data'][$k]['image'] = $misstion['image'];
                        if ($misstion['detail'] != NULL){
                            $text =  explode("|",$misstion['detail']);
                            $l =0;
                            foreach ($text as $text) {
                                $data['misstion']['data'][$k]["text_".$l] = $text;
                                $l++;
                            }  
                        }
                        $k++;
                    }
                }
                if ($pay != NULL) {
                    $data['pay']['title'] ="ค่าใช้จ่ายในการเรียนแพทย์ ต้องจ่ายเท่าไหร่ ?";
                    // $data['pay']['data'] = $pay;
                    $m=0;
                    foreach ($pay as $pay) {
                        $data['pay']['data'][$m]['title'] = $pay['titel'];
                        if ($pay['detail'] != NULL){
                            $text =  explode("|",$pay['detail']);
                            $n =0;
                            foreach ($text as $text) {
                                $data['pay']['data'][$m]["text_".$n] = $text;
                                $n++;
                            }  
                        }
                        $m++;
                    }
                }
                $question = $this->course->getQuestionType($department['id']);
                if ($question != NULL) {
                    $data['question']['title'] ="Frequently Asked Questions";
                    $m=0;
                    $data['question']['type'] = $question;
                    foreach ($question as $question) {
                        $question_text = $this->course->getQuestion($question['id']);
                        $data['question']['type'][$m]['question'] = $question_text;
                        $m++;
                    }
                }
                if($department['id'] == 9) {
                    $c = $this->course->getCrepes();
                    $data['crepes'] = $c;
                }
                if($department['id'] == 7){
                    $c = $this->course->getShouldContact($department['id']);
                    $data['should_contact'] = $c;
                }
                if($contract != NULL){
                    $data['contract'] = $contract;
                }
            }
            
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data 
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function branch_get()
    {
        $branch = $this->get('branch');
        $content = $this->course->getBranch($branch);
       // $data = $content;
        foreach ($content as $content) {
            $icon = $this->course->getBranchIcon($content["courseID"]);
            $data['information'] = $content;
            if ($content['nameDegreeTH']){
                $text =  explode("|",$content["nameDegreeTH"]);
                $i = 0;
                if (count($text)>0){
                    foreach ($text as $tex) {
                        $data['vision']["vision_".$i] = $text[$i]; 
                        $i++;
                    }
                }else {
                    $data['vision'] = [];
                }
                
            }
            if ($content['vision']){
                $text =  explode("|",$content["vision"]);
                $i = 0;
                if (count($text)>0){
                    foreach ($text as $tex) {
                        $data['vision']["vision_".$i] = $text[$i]; 
                        $i++;
                    }
                }else {
                    $data['vision'] = [];
                }
                
            }
            if ($content['mission']){
                $text =  explode("|",$content["mission"]);
                $i = 0;
                if (count($text)>0){
                    foreach ($text as $tex) {
                        $data['mission']["mission_".$i] = $text[$i]; 
                        $i++;
                    }
                }else {
                    $data['mission'] = [];
                }
                
            }
            if ($content['image']){
                $text =  explode(",",$content["image"]);
                $i = 0;
                if (count($text)>0){
                    foreach ($text as $tex) {
                        $data['image']["image_".$i] = $text[$i]; 
                        $i++;
                    }
                }else {
                    $data['image'] = [];
                }
                
            }
            if ($content['footer']){
                $text =  explode(",",$content["footer"]);
                $i = 0;
                if (count($text)>0){
                    foreach ($text as $tex) {
                        $data['footer']["image_".$i] = $text[$i]; 
                        $i++;
                    }
                }else {
                    $data['footer'] = [];
                }
                
            }
            $data['icon'] =  $icon;
        }
        $header = array("ชื่อปริญญาและสาขา","วิสัยทัศน์","พันธกิจ","โครงสร้างหลักสูตร","แผนการศึกษา","ผลการรับและสำเร็จการศึกษา","เป้าหมายการรับและการสำเร็จการศึกษา","ผลการรับและสำเร็จการศึกษา","เป้าหมายการได้งานทำของบัณฑิต");
        $i = 0;
        foreach ($header as $header) {
            $data["header"]["header_".$i] =  $header;
            $i++;
        }
        if ($data) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    
    public function branch_contract_get()
    {
        $c = $this->get('course');
        $content = $this->course->getBranchContractFooter($c);
        $data = $content;
        foreach ($content as $content) {
            $k =0 ;
            if($content['title'] != NULL){
                $text =  explode(",",$content["title"]);
                $i =0 ;
                if (count($text)>0){
                foreach ($text as $text) {
                        $data[$k]["title".$i] = $text;
                        $i++;
                    }
                }
            }
            $k++;
        }
        
        if ($data) {
            $this->response([
                'code' => 200,
                'status' => "Get data success",
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else {
            $this->response([
                'code' => 404,
                'status' => "Not found",
                'data' => []
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
}